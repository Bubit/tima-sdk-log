/*
The MIT License (MIT)

Copyright (c) 2015-2017 HyperTrack (http://hypertrack.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package com.hypertrack.hyperlog_demo;

import android.content.Context;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.tima.ai.hyperlog.HLCallback;
import com.tima.ai.hyperlog.HyperLog;
import com.tima.ai.hyperlog.error.HLErrorResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Created by Aman on 04/10/17.
 */

public class MainActivity extends AppCompatActivity {

    public static boolean bActivity = false;  // true-onResume()  false-onPause()
    private static final String TAG = MainActivity.class.getSimpleName();
    EditText editText, endPointUrl;
    ListView listView;
    List<String> logsList = new ArrayList<>();
    ArrayAdapter listAdapter;

    private HyperLog hyperLogDefault = HyperLog.getInstanceDefault();

  //  ThreadPushDeviceLogModel threadPushDeviceLogModel = new ThreadPushDeviceLogModel(this);


    int count = 0;
    String[] logs = new String[]{"Download Library", "Library Downloaded", "Initialize Library", "Library Initialized", "Log Message", "Message Logged",
            "Scroll to bottom", "View friends profile", "Add new friends", "Logs Pushed to Server", "Logs Deleted", "Register Account", "Buy new product", "Cancel buy product",
            "Close Ads View", "Add new card", "Open messenger"};
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Set Custom Log Message Format.

        // set user id if user id login on device
        HyperLog.getInstanceDefault().getAppLogModel().uid(UUID.randomUUID().toString());


        endPointUrl = (EditText) findViewById(R.id.end_point_url);
        editText = (EditText) findViewById(R.id.logText);
        listView = (ListView) findViewById(R.id.listView);
        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, logsList);
        listView.setAdapter(listAdapter);

        hyperLogDefault.setCallback(new HLCallback() {
            @Override
            public void onSuccess(@NonNull Object response) {
                Log.v("Response ", response.toString());
                showToast("Log pushed");
            }

            @Override

            public void onError(@NonNull HLErrorResponse HLErrorResponse) {
                Log.e("Response Error", HLErrorResponse.getErrorMessage());
            }
        });

    }



    public void setEndPoint(View view) {
        String url = endPointUrl.getText().toString();
        if (TextUtils.isEmpty(url)) {
            Toast.makeText(this, "Url can't be empty.", Toast.LENGTH_SHORT).show();
            return;
        }
        hyperLogDefault.setUrl(url);
    }


    public void addLog(View view) {
        if (!TextUtils.isEmpty(editText.getText().toString())) {

            hyperLogDefault.addBehaviorLog(TAG, editText.getText().toString());


            editText.getText().clear();
            if (count >= logs.length)
                count = 0;

            editText.setText(logs[count++]); // on demo next action in page
            showToast("Log Added");
            }
    }



    public void addAndForcePushLog(View view) {

        if (!TextUtils.isEmpty(editText.getText().toString())) {

            hyperLogDefault.addBehaviorLog(TAG, editText.getText().toString());
            hyperLogDefault.pushLogToServer();

            editText.getText().clear();
            if (count >= logs.length)
                count = 0;

            editText.setText(logs[count++]);
            showToast("Log Added");
        }

    }


    private void showToast(String message) {
        if (toast != null)
            toast.cancel();
        toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onResume(){
        super.onResume();
        hyperLogDefault.startServiceLog();
        bActivity = true;
    }

    @Override
    protected void onPause(){
        super.onPause();
        hyperLogDefault.stopServiceLog();
        bActivity = false;
    }
}