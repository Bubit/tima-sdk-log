package com.tima.ai.hyperlog.exception;

public class RepositoriesUpdateException extends HyperLogException {

    public RepositoriesUpdateException(Exception e) {
        super(e, "Update log into data base error");
    }
}
