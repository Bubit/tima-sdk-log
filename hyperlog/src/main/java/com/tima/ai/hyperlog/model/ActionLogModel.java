package com.tima.ai.hyperlog.model;

public class ActionLogModel extends LogModel {
    protected int typeLogId;

    public ActionLogModel(int typeLogId) {
        this.typeLogId = typeLogId;
    }

    public int getTypeLogId() {
        return typeLogId;
    }
}
