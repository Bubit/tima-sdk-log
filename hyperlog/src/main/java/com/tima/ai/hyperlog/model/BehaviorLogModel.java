
/*
The MIT License (MIT)

Copyright (c) 2015-2017 HyperTrack (http://hypertrack.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package com.tima.ai.hyperlog.model;

/**
 * Created by datluyen on 24/04/2018.
 */


import com.google.gson.annotations.SerializedName;

/** package
 *
 * Device log include default device log and array off action log
 *
 * */
public class BehaviorLogModel extends ActionLogModel {

    @SerializedName("k")
    private String key;
    @SerializedName("v")
    private String value;
    @SerializedName("t")
    private long timeStamp;
    @SerializedName("o")
    private String option;

    public BehaviorLogModel(String key, String value, String option) {
        super(TYPE_ID_BEHAVIOR_LOG);
        this.key = key;
        this.value = value;
        this.timeStamp = System.currentTimeMillis();
        this.option = option;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }



    @Override
    public String toString() {
        return "DeviceLogModel{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                ", timeStamp=" + timeStamp +
                ", option='" + option + '\'' +
                '}';
    }
}