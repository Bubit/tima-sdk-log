

package com.tima.ai.hyperlog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;


import com.android.volley.Request;
import com.tima.ai.hyperlog.error.HLErrorResponse;
import com.tima.ai.hyperlog.exception.HyperLogException;
import com.tima.ai.hyperlog.loghelper.BehaviorLogMemoryHelper;
import com.tima.ai.hyperlog.model.AppLogModel;
import com.tima.ai.hyperlog.model.BehaviorLogModel;
//import com.tima.ai.hyperlog.model.OtherLogModel;
import com.tima.ai.hyperlog.request.ActionLogRequest;
import com.tima.ai.hyperlog.utils.LocationTracker;
import com.tima.ai.hyperlog.utils.VolleyUtils;
import com.tima.ai.hyperlog.utils.LogModelParse;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map;

import java.lang.String;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Aman on 04/10/17.
 */
public class HyperLog {

    private static final String TAG = "HyperLog";

    private static final int MAX_STORE_LOG=10000;
    private static final Random randomKeyBackgroundLog=new Random();


    protected static HyperLog hyperLog;

    public static HyperLog getInstanceDefault(){
        return hyperLog;
    }

    public static HyperLog getOrNewInstanceDefault(String url, String appKey, Context context) throws HyperLogException {
        if(hyperLog==null) {
            hyperLog= new HyperLog();
            hyperLog.initialize(url, appKey, context);
        }
        return hyperLog;
    }

    private static final String LOCAL_LOG_KEY="PrefLOG";

    private BehaviorLogMemoryHelper logMemoryHelper= new BehaviorLogMemoryHelper();

    private String url;

    private Context contextThis;

    private AppLogModel appLogModel;

    private SharedPreferences sharedPreferencesLog;

    private LocationTracker locationTracker;

    private WifiManager wifiManager;

    private Timer scheduleScanWifi;

    private HLCallback callback;

    public void initialize(String url, String appKey, Context context) throws HyperLogException {
        initialize(url, appKey, context, true);
    }

    public void initialize(String url, String appKey, Context context, boolean startServiceLog) throws HyperLogException {
        if(context==null)
            throw new HyperLogException("Context couldn't be null");
        if (TextUtils.isEmpty(url)) {
            throw new HyperLogException("Url host couldn't be empty!");
        }
        this.url=url;
        contextThis = context.getApplicationContext();

        appLogModel = new AppLogModel(context, appKey);


        // push logs are saved in Shared Preference to url
        sharedPreferencesLog = contextThis.getSharedPreferences(LOCAL_LOG_KEY, MODE_PRIVATE);
        pushLogInStore();

        // init service log of device

        if (startServiceLog) startServiceLog();

    }

    public void startServiceLog(){
        scanLocation();
        scanWifiName();
    }

    private void pushLogInStore(){
        Timer timer= new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try{
                    if(sharedPreferencesLog!=null && sharedPreferencesLog.getAll().size()>0){
                        for (Map.Entry<String, ?> entry : sharedPreferencesLog.getAll().entrySet()) {
                            VolleyUtils.addToRequestQueue(contextThis,new ActionLogRequest(Request.Method.POST,url,entry.getValue().toString(),null),TAG);
                        }
                        sharedPreferencesLog.edit().clear().apply();
                    }
                } catch (Exception e) {
                    Log.e("push background log",e.getMessage());
                }
            }
        }, 5000);

    }

    private void scanLocation(){
        if(locationTracker==null){
            locationTracker= new LocationTracker(contextThis, new LocationTracker.OnLocationUpdate() {
                @Override
                public void onUpdate(Location location) {
                    if(location!=null){
                        appLogModel.addDefaultInfo("longitude",location.getLongitude()+"");
                        appLogModel.addDefaultInfo("latitude",location.getLatitude()+"");
                    } else {
                        appLogModel.addDefaultInfo("longitude",null);
                        appLogModel.addDefaultInfo("latitude",null);
                    }
                }
            });
        }
        locationTracker.scanLocation();
    }

    private void scanWifiName(){
        if(wifiManager==null) wifiManager = (WifiManager)  contextThis.getSystemService(Context.WIFI_SERVICE);
        if (scheduleScanWifi==null){
            scheduleScanWifi = new Timer();
            scheduleScanWifi.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        if(wifiManager.isWifiEnabled()){
                            ArrayList<String> listWifi = new ArrayList<>();
                            wifiManager.startScan();
                            contextThis.registerReceiver(new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    try{
                                        List<ScanResult> results = wifiManager.getScanResults();
                                        for (ScanResult scanResult : results) {
                                            listWifi.add(scanResult.SSID + " - " + scanResult.capabilities);
                                        }
                                        appLogModel.addDefaultInfo("wfList",listWifi.toString());
                                        listWifi.clear();
                                    } catch (Exception e){
                                        Log.e("wifi scan" , e.getMessage());
                                    }
                                }
                            },  new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

                        }
                    } catch (Exception e){
                        Log.e("wifi scan", e.getMessage());
                    }
                }
            },0,30000);
        }
    }


    public void setCallback(HLCallback callback){
        this.callback=callback;
    }

//    public void addActionLogModel(ActionLogModel actionLogModel){
//        co the mo rong sau nay khi co nhieu loai log action khac nhau
//        BehaviorLogModel cung la 1 ActionLogModel
//    }
    
    public void addBehaviorLog(BehaviorLogModel logModel){
        try {

            if(TextUtils.isEmpty(this.url) || contextThis==null)
                throw new HyperLogException("HyperLog missing url or context. Sure you have initialized HypeLog ");
            logMemoryHelper.addLog(logModel);

            pushIfReady();

        } catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
    }

    public void addBehaviorLog(String key, String valueMess, String option) {
        addBehaviorLog(new BehaviorLogModel(key, valueMess, option));
    }

    public void addBehaviorLog(String key, String valueMess) {
        addBehaviorLog(key, valueMess, null);
    }

    public void pushLogToServer() {
        try {
            if(TextUtils.isEmpty(this.url) || contextThis==null)
                throw new HyperLogException("HyperLog missing url or context. Sure you have initialized HypeLog");

            if(logMemoryHelper.getLogCount()<=0) return;

            // poll log from all helper to model
            appLogModel.addAllBehaviorLogModels(logMemoryHelper.pollAll());

            // push
            if(( (ConnectivityManager)contextThis.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                VolleyUtils.addToRequestQueue(contextThis, new ActionLogRequest(Request.Method.POST, url, appLogModel, this.callback), TAG);
            }
            else {
                SharedPreferences sharedPreferences = contextThis.getSharedPreferences(LOCAL_LOG_KEY, MODE_PRIVATE);
                if(sharedPreferences.getAll().size() < MAX_STORE_LOG){
                    sharedPreferences.edit().putString(randomKeyBackgroundLog.nextLong()+"", LogModelParse.getInstance().toJson(appLogModel)).apply();
                }

            }

            appLogModel.clearBehaviorLog();

        } catch (HyperLogException e){
            callback.onError(new HLErrorResponse(e.getMessage()));
        }
    }

    private void pushIfReady(){
        if(logMemoryHelper.isReadyPush()) {
            pushLogToServer();
        }
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setContext(Context context) {
        this.contextThis = context;
    }

    public AppLogModel getAppLogModel() {
        return appLogModel;
    }

    public void stopServiceLog(){
        if(locationTracker!=null) locationTracker.stopScan();
        if(scheduleScanWifi!=null) {
            scheduleScanWifi.cancel();
            scheduleScanWifi=null;
        }
    }

    /**
     * Call this method to initialize HyperLog.
     * By default, seven days older logs will get expire automatically. You can change the expiry
     * period of logs by defining expiryTimeInSeconds.
     *
     * @param context             The current context.
     */

    /*
     * you should be getInstanceDefault().initialize(...) or getOrNewInstance(...)
     * */

    @Deprecated
    public static void initialize(@NonNull Context context, String appKey) {
        try {
            getInstanceDefault().initialize("null", appKey, context);
        } catch (HyperLogException e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public static void setURL(String url) {
        getInstanceDefault().setUrl(url);
    }

    @Deprecated
    public static String getURL() {
        return getInstanceDefault().url;
    }

    @Deprecated
    public static boolean hasPendingDeviceLogs() {
        return getInstanceDefault().logMemoryHelper.getLogCount()>0;
    }

    @Deprecated
    public static void addStaticInfo(String key, String value){
        getInstanceDefault().appLogModel.addDefaultInfo(key, value);
    }

    /**
     * Add log to queue and push to server.
     * @param mContext
     * @param callback
     * @param actionName : Action key
     * @param value : Action value
     * @param optional : if we want to store more information. Please set in this field
     */

    @Deprecated
    public static void addMessagesAndForcePush(Context mContext, final HLCallback callback, String actionName, String value, String optional) {
        HyperLog hyperLog= getInstanceDefault();
        hyperLog.setCallback(callback);
        hyperLog.addBehaviorLog(actionName, value, optional);
        hyperLog.pushLogToServer();
    }

    @Deprecated
    public static void addMessagesAndForcePush(Context mContext, final HLCallback callback, String actionName,
                                               String value) {
        addMessagesAndForcePush(mContext, callback, actionName, value, null);
    }

    /**
     * Add log into queue. Then system auto check if queue size if over threshold config
     * @param mContext
     * @param callback
     * @param actionName : Action key
     * @param value : Action value
     * @param optional : if we want to store more information. Please set in this field
     */

    @Deprecated
    public static void addMessages(Context mContext, HLCallback callback, String actionName, String value, String optional) {
        HyperLog hyperLog= getInstanceDefault();
        hyperLog.setCallback(callback);
        hyperLog.addBehaviorLog(actionName, value, optional);
    }

    @Deprecated
    public static void addMessages(Context mContext, HLCallback callback, String actionName, String value) {
        addMessages(mContext, callback, actionName, value, null);
    }

}