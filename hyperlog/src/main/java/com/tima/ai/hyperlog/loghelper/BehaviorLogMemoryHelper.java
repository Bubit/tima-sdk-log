package com.tima.ai.hyperlog.loghelper;

import android.util.Log;

import com.tima.ai.hyperlog.model.BehaviorLogModel;
import com.tima.ai.hyperlog.model.LogModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BehaviorLogMemoryHelper implements LogSourceHelper<BehaviorLogModel> {

    public int MAX_QUEUE_SIZE = 1000;
    public int PUSH_THRESHOLD = 10;

    public static final String TAG = "C-QUEUE";

    public ConcurrentLinkedQueue<BehaviorLogModel> cQueue = new ConcurrentLinkedQueue<BehaviorLogModel>();

    public BehaviorLogMemoryHelper(int MAX_QUEUE_SIZE){

        this.MAX_QUEUE_SIZE = MAX_QUEUE_SIZE;

    }


    public BehaviorLogMemoryHelper(){ }

    @Override
    public long getLogCount() {
        return cQueue.size();
    }

    @Override
    public void addLog(BehaviorLogModel deviceLog) {

        if (cQueue.size() > MAX_QUEUE_SIZE){
            Log.i(TAG, " Queue is limited remove data from queue first");
            cQueue.poll();
        }
        cQueue.add(deviceLog);
    }

    @Override
    public void deleteLog(List<BehaviorLogModel> deviceLogList) {
        for (LogModel deviceLogModel : deviceLogList){
            cQueue.remove(deviceLogModel);
        }
    }

    @Override
    public void deleteAllDeviceLogs() {
        cQueue.clear();
    }

    @Override
    public List<BehaviorLogModel> pollAll() {
        List<BehaviorLogModel>  result = new ArrayList<>();
        while (!cQueue.isEmpty()){
            result.add(cQueue.poll());
        }
        return result;
    }

    @Override
    public List<BehaviorLogModel> poll(int size) {
        List<BehaviorLogModel>  result = new ArrayList<>();
        for(int i =0; !cQueue.isEmpty() && i < size; i++){
            result.add(cQueue.poll());
        }
        return result;
    }

    @Override
    public void clearOldLogs(int expiryTimeInSeconds) {
        // don't need this function
        cQueue.clear();
    }

    @Override
    public boolean isReadyPush(){
        return cQueue.size() >= PUSH_THRESHOLD;
    }
}
