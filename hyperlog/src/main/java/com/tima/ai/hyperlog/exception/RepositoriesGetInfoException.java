package com.tima.ai.hyperlog.exception;

public class RepositoriesGetInfoException extends HyperLogException {

    public RepositoriesGetInfoException(Exception e) {
        super(e, "Get repositories info error : " + e.getMessage());
    }
}
