package com.tima.ai.hyperlog.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.tima.ai.hyperlog.model.LogModel;

public class LogModelParse {
    public static String TAG = "ActivityLogModelUtils";

    private static LogModelParse logModelParse;

    public static LogModelParse getInstance(){
        if (logModelParse==null){
            logModelParse= new LogModelParse();
        }
        return logModelParse;
    }

    protected Gson gsonProvider = null;

    protected static String empty="";

    private Gson getJsonParse(){
        if (gsonProvider == null) {
            gsonProvider = new Gson();
        }
        return gsonProvider;
    }

    public String toJson(LogModel logModel) {
        try {
            String rs = getJsonParse().toJson(logModel);
            return rs;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return empty;
    }
}
