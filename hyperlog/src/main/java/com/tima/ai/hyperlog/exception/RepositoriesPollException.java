package com.tima.ai.hyperlog.exception;

public class RepositoriesPollException extends HyperLogException {

    public RepositoriesPollException(Exception e) {
        super(e, "Poll messages from repositories");
    }
}
