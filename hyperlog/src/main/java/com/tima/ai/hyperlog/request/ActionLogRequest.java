package com.tima.ai.hyperlog.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tima.ai.hyperlog.HLCallback;
import com.tima.ai.hyperlog.error.HLErrorResponse;
import com.tima.ai.hyperlog.model.AppLogModel;
import com.tima.ai.hyperlog.utils.LogModelParse;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ActionLogRequest extends StringRequest {

    private static final String TAG = ActionLogRequest.class.getSimpleName();

    private String requestBody;

    public static Map<String, String> header = null;

    public ActionLogRequest(int method, String url, AppLogModel logModel, HLCallback callback) {

        super(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(callback!=null)
                    callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback!=null)
                    callback.onError(new HLErrorResponse(error));
            }
        });
        this.requestBody = LogModelParse.getInstance().toJson(logModel);
    }

    public ActionLogRequest(int method, String url, String requestBody, HLCallback callback) {

        super(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(callback!=null)
                    callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback!=null)
                    callback.onError(new HLErrorResponse(error));
            }
        });
        this.requestBody = requestBody;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        if (header == null){
            header = new HashMap<>();
            header.putAll(super.getHeaders());
            header.put("Content-Encoding", "gzip");
        }
        return header;
    }

    @Override
    public String getBodyContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    public byte[] getBody() {
        try {
            Log.v("request", requestBody);
            return requestBody == null ? null : requestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            Log.e(TAG, "Unsupported Encoding while trying to get the bytes " + uee.getMessage());
            return null;
        }
    }
}