package com.tima.ai.hyperlog.model;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.tima.hyperlog.BuildConfig;


import java.util.ArrayList;
import java.util.List;

public class AppLogModel extends LogModel{

    @SerializedName("df")
    // log static info
    public JsonObject defaultLog = new JsonObject();

    @SerializedName("as")
    // log behavior
    public List<BehaviorLogModel> behaviorLog = new ArrayList<>();

    public AppLogModel(Context context, String appKey){
        defaultLog.addProperty("vc", BuildConfig.VERSION_CODE);
        defaultLog.addProperty("aid", BuildConfig.APPLICATION_ID);
        String deviceId = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        defaultLog.addProperty("iid", deviceId);
        if(!TextUtils.isEmpty(appKey))defaultLog.addProperty("appk", appKey);
    }


    public void uid(String uid){
        defaultLog.addProperty("uid",uid);
    }

    public JsonObject getDefaultLog() {
        return defaultLog;
    }

    public void setDefaultLog(JsonObject defaultLog) {
        this.defaultLog = defaultLog;
    }


    public List<BehaviorLogModel> getBehaviorLogModels() {
        return behaviorLog;
    }

    public void addAllBehaviorLogModels(List<BehaviorLogModel> behaviorLog) {
        this.behaviorLog.addAll(behaviorLog);
    }

    public void addDefaultInfo(String key, String value){
        defaultLog.addProperty(key, value);
    }

    public void addDefaultInfo(String key, long value){
        defaultLog.addProperty(key, value);
    }

    public void addDefaultInfo(String key, int value){
        defaultLog.addProperty(key, value);
    }


    public void addBehaviorLog(BehaviorLogModel behaviorLogModel){
        this.behaviorLog.add(behaviorLogModel);
    }

    public void clearBehaviorLog(){
        this.behaviorLog.clear();
    }

    @Override
    public String toString() {
        return "ActivityLogModel{" +
                "behaviorLogModels=" + behaviorLog +
                '}';
    }
}
