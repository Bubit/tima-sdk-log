package com.tima.ai.hyperlog.exception;

public class HyperLogException extends Exception{

    private String messages;

    public HyperLogException(Exception e, String messages){
        super(messages + " : " + e.getMessage(), e);
        this.messages = messages;
    }

    public HyperLogException(String message) {
        super(message);
    }
}
